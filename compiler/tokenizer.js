function tokenizer(input) {
  let current = 0;
  let tokens = [];
  const WHITESPACE_REGEXP = /\s/;
  const NUMBERS_REGEXP = /[0-9]/;
  const LETTERS_REGEXP = /[a-z]/i;

  while (current < input.length) {
    let currentChar = input[current];

    if (currentChar === "(") {
      tokens.push({ type: "paren", value: "(" });
      current++;
      continue;
    }

    if (currentChar === ")") {
      tokens.push({ type: "paren", value: ")" });
      current++;
      continue;
    }

    if (WHITESPACE_REGEXP.test(currentChar)) {
      current++;
      continue;
    }

    if (NUMBERS_REGEXP.test(currentChar)) {
      let value = "";

      while (NUMBERS_REGEXP.test(currentChar)) {
        value += currentChar;
        currentChar = input[++current];
      }

      tokens.push({ type: "number", value });
      continue;
    }

    if (LETTERS_REGEXP.test(currentChar)) {
      let value = "";

      while (LETTERS_REGEXP.test(currentChar)) {
        value += currentChar;
        currentChar = input[++current];
      }

      tokens.push({ type: "name", value });
      continue;
    }

    if (currentChar === '"') {
      let value = "";

      currentChar = input[++current];

      while (currentChar !== '"') {
        value += currentChar;
        currentChar = input[++current];
      }

      currentChar = input[++current];
      tokens.push({ type: "string", value });
      continue;
    }

    throw new TypeError(
      "I dont know what this currentCharacter is: " + currentChar
    );
  }

  return tokens;
}

module.exports = tokenizer;
