function parser(currentTokens) {
  let current = 0;

  const ast = { type: "Program", body: [] };

  while (current < currentTokens.length) {
    ast.body.push(walk());
  }

  return ast;

  function walk() {
    let currentToken = currentTokens[current];

    if (currentToken.type === "number") {
      current++;

      return { type: "NumberLiteral", value: currentToken.value };
    }

    if (currentToken.type === "string") {
      current++;

      return { type: "StringLiteral", value: currentToken.value };
    }

    if (currentToken.type === "paren" && currentToken.value === "(") {
      currentToken = currentTokens[++current];

      const node = { type: "CallExpression", name: currentToken.value, params: [] };

      currentToken = currentTokens[++current];

      while (
        currentToken.type !== "paren" ||
        (currentToken.type === "paren" && currentToken.value !== ")")
      ) {
        node.params.push(walk());
        currentToken = currentTokens[current];
      }

      current++;

      return node;
    }

    throw new TypeError(currentToken.type);
  }
}

module.exports = parser;
