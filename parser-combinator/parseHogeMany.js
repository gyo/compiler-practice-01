const parseHogeMany = (target, position) => {
  const result = [];

  for (;;) {
    if (target.substr(position, 4) === "hoge") {
      result.push("hoge");
      position += 4;
    } else {
      break;
    }
  }

  return [true, result, position];
};

console.log(parseHogeMany("hogehoge", 0));
console.log(parseHogeMany("hogehoge", 1));
console.log(parseHogeMany("ahogehoge", 1));
console.log(parseHogeMany("apple", 0));
