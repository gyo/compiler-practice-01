const token = str => {
  const len = str.length;

  return (target, position) => {
    if (target.substr(position, len) === str) {
      return [true, str, position + len];
    } else {
      return [false, null, position];
    }
  };
};

const many = parser => {
  return (target, position) => {
    const result = [];

    for (;;) {
      const parsed = parser(target, position);

      if (parsed[0]) {
        result.push(parsed[1]);
        position = parsed[2];
      } else {
        break;
      }
    }

    return [true, result, position];
  };
};

const choice = (...parsers) => {
  return (target, position) => {
    for (parser of parsers) {
      const parsed = parser(target, position);

      if (parsed[0]) {
        return parsed;
      }
    }

    return [false, null, position];
  };
};

const seq = (...parsers) => {
  return (target, position) => {
    const result = [];
    for (parser of parsers) {
      const parsed = parser(target, position);

      if (parsed[0]) {
        result.push(parsed[1]);
        position = parsed[2];
      } else {
        return [false, null, position];
      }
    }

    return [true, result, position];
  };
};

const option = parser => {
  return (target, position) => {
    const parsed = parser(target, position);
    if (parsed[0]) {
      return parsed;
    } else {
      return [true, null, position];
    }
  };
};

const regex = regexp => {
  if (regexp.source.substring(0, 1) !== "^") {
    const global = regexp.global ? "g" : "";
    const ignoreCase = regexp.ignoreCase ? "i" : "";
    const multiline = regexp.multiline ? "m" : "";
    regexp = new RegExp(
      `^(?:${regexp.source})`,
      `${global}${ignoreCase}${multiline}`
    );
  }

  return (target, position) => {
    regexp.lastIndex = 0;
    const regexResult = regexp.exec(target.slice(position));

    if (regexResult) {
      position += regexResult[0].length;
      return [true, regexResult[0], position];
    } else {
      return [false, null, position];
    }
  };
};

// console.log(many(token("hoge"))("hogehoge", 0));
// console.log(many(token("hoge"))("hogehoge", 1));
// console.log(many(token("hoge"))("ahogehoge", 1));
// console.log(many(token("hoge"))("apple", 0));
// console.log(choice(token("foo"), token("bar"))("foo", 0));
// console.log(choice(token("afoo"), token("bar"))("bar", 1));
// console.log(seq(token("foo"), choice(token("bar"), token("baz")))("foobar", 0));
// console.log(choice(token("foo"), token("bar"))("apple", 0));
// console.log(choice(token("foo"), token("bar"))("bar", 0));
// console.log(seq(token("foo"), choice(token("bar"), token("baz")))("foobaz", 0));
// console.log(seq(token("foo"), choice(token("bar"), token("baz")))("foo", 0));
// console.log(option(token("hoge"))("hoge", 0));
// console.log(option(token("hoge"))("ahoge", 1));
// console.log(option(token("hoge"))("hoge", 1));
// console.log(regex(/hoge/)("hoge", 0));
// console.log(regex(/[1-9][0-9]*/)("2014", 0));
// console.log(regex(/[1-9][0-9]*/)("0000", 0));
// console.log(regex(/[1-9][0-9]*/)("02014", 1));

const lazy = callback => {
  let parse;
  return (target, position) => {
    if (!parse) {
      parse = callback();
    }

    return parse(target, position);
  };
};

// const lazyMany = option(seq(token("hoge"), lazy(() => lazyMany)));
// console.log(lazyMany("hoge", 0));
// console.log(lazyMany("hogehoge", 0));
// console.log(lazyMany("hogehogehoge", 0));

const map = (parser, fn) => {
  return (target, position) => {
    const parsed = parser(target, position);
    if (parsed[0]) {
      return [parsed[0], fn(parsed[1]), parsed[2]];
    } else {
      return parsed;
    }
  };
};

// const mapToken = map(token("hello"), result => `${result}をパースしましたÏ`);
// console.log(mapToken("hello", 0));
// console.log(mapToken("apple", 0));

const char = str => {
  const dict = [];
  for (const char of [...str]) {
    dict[char] = char;
  }

  return (target, position) => {
    const char = target.substr(position, 1);

    if (dict[char]) {
      return [true, char, position + 1];
    } else {
      return [false, null, position];
    }
  };
};

// var anyChar = char("abcdef");
// console.log(anyChar("a", 0));
// console.log(anyChar("b", 0));
// console.log(anyChar("g", 0));
// console.log(anyChar("", 0));

///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

const parserFactory = (regExp, type) => {
  return (target, position) => {
    const regexResult = regExp.exec(target.slice(position));
    if (regexResult) {
      return [
        true,
        {
          type,
          value: regexResult[0]
        },
        position + regexResult[0].length
      ];
    }
    return [false, null, position];
  };
};
const numberParser = parserFactory(new RegExp(/^(?:0|[1-9][0-9]*)/), "number");
const operatorParser = parserFactory(new RegExp(/^[\+\-\*\/]/), "operator");
const startParenthesisParser = parserFactory(new RegExp(/^\(/), "number");
const endParenthesisParser = parserFactory(new RegExp(/^\)/), "number");
const lazyParenthesisParser = lazy(() => {
  return seq(startParenthesisParser, expression, endParenthesisParser);
});
const expression = seq(
  choice(numberParser, lazyParenthesisParser),
  many(seq(operatorParser, choice(numberParser, lazyParenthesisParser)))
);
const parse = target => {
  const parsed = expression(target, 0);
  if (!parsed[0]) {
    console.log("パース失敗");
  } else if (target.length !== parsed[2]) {
    console.log(`${1 + parsed[2]}文字目でパース失敗`);
  } else {
    console.log(JSON.stringify(parsed, null, "  "));
  }
};

// parse("1+2-(3+1");
// parse("hoge");
// parse("13+(2-1)");
// parse("0-3+(((3)))");
parse("1+2-3*4/5+(6-7)*(8/9+10)-(11*(12/13)+14)");
