const parseFooOrBar = (target, position) => {
  if (target.substr(position, 3) === "foo") {
    return [true, "foo", position + 3];
  }

  if (target.substr(position, 3) === "bar") {
    return [true, "bar", position + 3];
  }

  return [false, null, position];
};

console.log(parseFooOrBar("foo", 0));
console.log(parseFooOrBar("bar", 0));
console.log(parseFooOrBar("apple", 0));
