const parseHoge = (target, position) => {
  if (target.substr(position, 4) === "hoge") {
    return [true, "hoge", position + 4];
  } else {
    return [false, null, position];
  }
};

console.log(parseHoge("hoge", 0));
console.log(parseHoge("hoge", 1));
console.log(parseHoge("ahoge", 1));
console.log(parseHoge("apple", 0));
