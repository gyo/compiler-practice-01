# コンパイラの勉強

[JavaScript でパーサコンビネータのコンセプトを理解する(「正規表現だけに頼ってはいけない」の続き) - id:anatoo のブログ](http://blog.anatoo.jp/entry/2015/04/26/220026)

[jamiebuilds/the-super-tiny-compiler: Possibly the smallest compiler ever](https://github.com/jamiebuilds/the-super-tiny-compiler)

## Stages of a Compiler

1. Parsing
   1. Lexical Analysis
      ：Raw code を Token に変換する
   1. Syntactic Analysis
      ：Token を AST(Abstract Syntax Tree) に変換する
1. Transformation
1. Code Generation

## Tokenizer

We're gonna start off with our first phase of parsing, lexical analysis, with the tokenizer.

We're just going to take our string of code and break it down into an array of tokens.

正規表現とかを使って、文字列を配列にするだけ。
